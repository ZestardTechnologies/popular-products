<script> 
	<?php 
		echo "var product_ids, product_id, product_page=1, cart_page=0;
		{% if template contains 'product' %}  		
			product_id = {{ product.id }};           
		{% else %}    	        
			product_page=0;  
		{% endif %}
		{% if template contains 'cart' %}  		
			cart_page = 1;           
		{% else %}        
			cart_page = 0;  
		{% endif %}
		var shop_name = '{{ shop.domain }}';"				
	?>
</script>

<div id="most_view_div_box">
	<div id="most_view_js"></div>
	<div class="col-sm-12 form-group text-center" id="most_view_slider">
	  <br>
		<h1 class="preview_title">
		</h1>
		<h4 class="preview_subtitle">
		</h4>						
		<div class="most_view flexslider carousel"> 
			<ul class='slides popular_products'>		
			</ul>
		</div>
	</div>
	<div class="popup" id="most_view_popup_modal" data-popup="popup-4" style="width:100%;z-index:999999999999999;display:none;top:25%position:absolute;">
		<div class="popup-inner" id="most_view_content" style="z-index:999999999999999;height:500px;overflow: scroll;">  
			<div class="container">
				<div class="col-md-6" style="float:left;width:28%">
					<img id="most_view_image" style="height:320px;width:310px" class='product_click' src=''/>				
				</div>
				<div id="product_content" class="col-md-6" style="float:right;width:72%">
					<h2 id="product_name"></h2>              	
					<h4 id="product_price" style="float:left;width:50%"></h4>
					<div id="select_variant_div" style="float:left;width:50%"></div> 
					<br><br>
					<div class="text-left" id="add_to_cart_button"></div>
					<br>
					<h5 id="product_desc"></h5>
				</div>
			</div>    
			<!--p class="text-right"><a data-popup-close="popup-2" href="#">Close</a></p-->
		<a class="popup-close" data-popup-close="popup-4" href="#">x</a>
		</div>
	</div>

	<?php echo "{{ 'https://zestardshop.com/shopifyapp/popular_products/public/css/flexslider.css' | stylesheet_tag }}" ?>
	<?php echo "{{ 'https://zestardshop.com/shopifyapp/popular_products/public/css/mdb/font-awesome.min.css' | stylesheet_tag  }}" ?>
	<?php echo "{{ 'https://zestardshop.com/shopifyapp/popular_products/public/css/custom_modal.css' | stylesheet_tag  }}" ?>
	<?php echo "{{ 'https://zestardshop.com/shopifyapp/popular_products/public/css/mdb/font-awesome.min.css' | stylesheet_tag  }}" ?>
	<?php echo "{{ 'https://zestardshop.com/shopifyapp/popular_products/public/css/style.css' | stylesheet_tag  }}" ?>
	<?php echo "{{ 'https://zestardshop.com/shopifyapp/popular_products/public/js/most_view.js' | script_tag }}"; ?>
	<?php echo "{{ 'https://zestardshop.com/shopifyapp/popular_products/public/css/jquery.modal.min.css'| stylesheet_tag }}"; ?>

</div>
