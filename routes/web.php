<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('dashboard', 'callbackController@dashboard')->name('dashboard');

Route::get('callback', 'callbackController@index')->name('callback');

Route::get('redirect', 'callbackController@redirect')->name('redirect');

Route::get('uninstall', 'callbackController@uninstall')->name('uninstall');

Route::any('update-modal-status', 'callbackController@update_modal_status')->name('update-modal-status');

Route::get('payment_process', 'callbackController@payment_method')->name('payment_process');

Route::get('payment_success/{shop}', 'callbackController@payment_compelete')->name('payment_success');

Route::get('preview', 'previewController@index')->middleware('cors')->name('preview');

Route::any('save-quick-buy', 'PopularProductsController@save_quick_buy')->name('save-quick-buy');

Route::any('get-settings', 'PopularProductsController@get_settings')->middleware('cors')->name('get-settings');

Route::any('get-details', 'PopularProductsController@get_details')->middleware('cors')->name('get-details');

Route::any('get-products', 'PopularProductsController@get_products')->middleware('cors')->name('get-products');

Route::any('testing', 'PopularProductsController@testing')->middleware('cors')->name('testing');

Route::any('products', 'PopularProductsController@products')->name('products');

Route::any('save-session', 'PopularProductsController@save_session')->name('save-session');

Route::any('save-products', 'PopularProductsController@save_products')->name('save-products');

Route::any('acknowledge', 'PopularProductsController@acknowledge')->name('acknowledge');

Route::any('view-product', 'PopularProductsController@save_product_count')->name('view-product');

Route::any('update-ids', 'PopularProductsController@update_product_ids')->middleware('cors')->name('update-ids');

Route::any('track-products', 'PopularProductsController@track_products')->name('track-products');

Route::any('get-popular-products', 'PopularProductsController@track_popular_products_data')->name('get-popular-products');

Route::get('help', function () {
    return view('help');
})->name('help');

Route::any('charge-declined', 'callbackController@charge_declined')->middleware('HandlePreflight')->name('charge-declined');

/* For Best Seller */
Route::any('get-seller-data', 'PopularProductsController@get_bestseller_products')->middleware('cors')->name('get-seller-data');
Route::any('get-product-data', 'PopularProductsController@get_product_data')->middleware('cors')->name('get-product-data');
Route::any('best-seller', 'PopularProductsController@get_best_seller_view')->middleware('cors')->name('best-seller');



/* Route::get('track-products', function () {
    return view('track_products');
})->name('track-products'); */

/* For Most Viewed Products */
Route::any('get-most-product', 'PopularProductsController@get_popular_products')->middleware('cors')->name('get-most-product');

/* For Recently Viewed Products */
Route::any('recent-product', 'PopularProductsController@save_recently_viewed_product')->middleware('cors')->name('recent-product');
