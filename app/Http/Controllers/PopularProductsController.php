<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\ShopModel;
use App\ProductsModel;
use App;
use DB;
use File;
use Mail;

class PopularProductsController extends Controller {

    public function get_details(Request $request) {
        echo $request->input('shop');
    }

    public function save_quick_buy(Request $request) 
    {
        $shop = session('shop');
        if (!empty($_GET['shop'])) {
            $shop = $_GET['shop'];
        }

        $app_settings = DB::table('appsettings')->where('id', 1)->first();
        $select_store = DB::table('usersettings')->where('store_name', $shop)->get();
        $sh = App::make('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' =>
                    $app_settings->shared_secret, 'SHOP_DOMAIN' => $shop, 'ACCESS_TOKEN' =>
                    $select_store[0]->access_token]);
        $currency = $sh->call(['URL' => '/admin/shop.json', 'METHOD' => 'GET']);
        $shop_currency = $currency->shop->currency;
        $count = DB::table('popular_products_settings')->where('store', $shop)->count();

        if ($request->input('status')) {
            $status = 1;
        } else {
            $status = 0;
        }
        if ($request->input('select_products')) {
            $select_products = 1;
        } else {
            $select_products = 0;
        }
        if ($request->input('autoplay')) {
            $autoplay = 1;
        } else {
            $autoplay = 0;
        }
        if ($request->input('loop')) {
            $loop = 1;
        } else {
            $loop = 0;
        }
        if ($request->input('border_status')) {
            $border_status = 1;
        } else {
            $border_status = 0;
        }
        if ($count > 0) {
            $settings = array(
                'app_status' => $status,
                'slider_title' => $request->input('slider_title'),
                'slider_subtitle' => $request->input('slider_subtitle'),
                'number_of_products' => $request->input('product_no'),
                'autoplay_slider' => $autoplay,
                'display_border' => $border_status,
                'border_style' => $request->input('border_style'),
                'border_color' => $request->input('border_color'),
                'border_size' => $request->input('border_size'),
                'loop' => $loop,
                'product_click' => $request->input('product_click')
            );
            DB::table('popular_products_settings')->where('store', $shop)->update($settings);
        } else {
            
            $settings = array(
                'store' => $shop,
                'app_status' => $status,
                'slider_title' => $request->input('slider_title'),
                'slider_subtitle' => $request->input('slider_subtitle'),
                'number_of_products' => $request->input('product_no'),
                'autoplay_slider' => $autoplay,
                'display_border' => $border_status,
                'border_style' => $request->input('border_style'),
                'border_color' => $request->input('border_color'),
                'border_size' => $request->input('border_size'),
                'loop' => $loop,
                'product_click' => $request->input('product_click'),
                'shop_currency' => $shop_currency
            );
            
            DB::table('popular_products_settings')->insert($settings);
        }
        $notification = array(
            'message' => 'Settings Saved Successfully.',
            'alert-type' => 'success');
        return redirect()->route('dashboard')->with(['notification' => $notification, 'shop' => $shop]);
    }

    public function track_products(Request $request) {
        return view('track_products');
    }

    public function track_popular_products_data(Request $request) {
        $shop = session('shop');
        $products = ProductsModel::where('shop_name', $shop)->orderBy('count', 'desc')->limit(6)->get();
        $popular_products = array();
        $popular_products_id = array();
        $temp_array = array();
        if (count($products) > 0) {
            foreach ($products as $product) {
                $temp_array[$product->product_id] = $product->count;
                array_push($popular_products_id, $product->product_id);
            }
            /* 	
              $products = array_count_values($popular_products_id);
              arsort($products);
              $product_ids = array();
              foreach($products as $key=>$value)
              {
              array_push($product_ids, $key);
              }
              if(count($product_ids) < 6)
              {
              $ids = implode(",",$product_ids);
              }
              else
              {
              $ids = implode(",",$popular_products_id);
              }
             */
            $ids = implode(",", $popular_products_id);
            $app_settings = DB::table('appsettings')->where('id', 1)->first();
            $select_store = DB::table('usersettings')->where('store_name', $shop)->get();
            $sh = App::make('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' =>
                        $app_settings->shared_secret, 'SHOP_DOMAIN' => $shop, 'ACCESS_TOKEN' =>
                        $select_store[0]->access_token]);
            $shopify_products = $sh->call(['URL' => '/admin/products.json?ids=' . $ids . "&published_status=published", 'METHOD' => 'GET']);
            $i = 0;
            foreach ($shopify_products as $product) {
                foreach ($product as $attributes) {
                    $data["product_id"] = $attributes->id;
                    $data["product_name"] = $attributes->title;
                    $data["view_count"] = $temp_array[$attributes->id];
                    array_push($popular_products, $data);
                }
            }
            echo json_encode($popular_products);
        }
    }

    public function get_settings(Request $request) {
        $shop = $request->input('shop_name');
        $settings = DB::table('popular_products_settings')->where('store', $shop)->get();
        return json_encode($settings);
    }

    public function get_product_data(Request $request) {
        $product_data = array();
        $app_settings = DB::table('appsettings')->where('id', 1)->first();
        $shop = session('shop');
        $select_store = DB::table('usersettings')->where('store_name', $shop)->get();
        $sh = App::make('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' =>
                    $app_settings->shared_secret, 'SHOP_DOMAIN' => $shop, 'ACCESS_TOKEN' =>
                    $select_store[0]->access_token]);
        $products = array_count_values($request->input('products'));
        arsort($products);
        $product_ids = array();
        foreach ($products as $key => $value) {
            array_push($product_ids, $key);
        }
        if (count($product_ids) < 6) {
            $ids = implode(",", $product_ids);
        } else {
            $ids = implode(",", array_slice($product_ids, 0, 6));
        }
        $variants = array();
        DB::table('best_seller_products')->where("store", $shop)->delete();
        $shopify_products = $sh->call(['URL' => '/admin/products.json?ids=' . $ids . "&published_status=published", 'METHOD' => 'GET']);
        $i = 0;
        foreach ($shopify_products as $product) {
            foreach ($product as $attributes) {
                $data = array(
                    'product_name' => $attributes->title,
                    'quantity_sold' => $products[$attributes->id]
                );
                array_push($product_data, $data);
            }
        }
        echo json_encode($product_data);
    }

    /* For Most Viewed Prodcuts App */

    public function save_product_count(Request $request) {
        $product_id = $request->input('product_id');
        $shop_name = $request->input('shop_name');
        $product = DB::table("popular_products")->where(['product_id' => $product_id, 'shop_name' => $shop_name])->first();
        if (count($product) > 0) {
            $count = $product->count + 1;
            DB::table("popular_products")->where('product_id', $product_id)->update(['count' => $count]);
        } else {
            $data = array(
                'shop_name' => $shop_name,
                'product_id' => $product_id,
                'count' => 1
            );
            DB::table("popular_products")->insert($data);
        }
    }

    public function get_popular_products(Request $request) 
    {
        $shop = $request->input('shop_name');
        $product_data = DB::table('popular_products')->where('shop_name', $shop)->orderBy('count', 'desc')->limit(6)->get();
        $ids = array();
        if (count($product_data) > 0) {
            foreach ($product_data as $product) {
                array_push($ids, $product->product_id);
            }
            $product_ids = implode(",", $ids);
            $app_settings = DB::table('appsettings')->where('id', 1)->first();
            $select_store = DB::table('usersettings')->where('store_name', $shop)->get();
            $sh = App::make('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' => $app_settings->shared_secret, 'SHOP_DOMAIN' => $shop, 'ACCESS_TOKEN' => $select_store[0]->access_token]);
            $products = $sh->call(['URL' => '/admin/products.json?ids=' . $product_ids . "&published_status=published", 'METHOD' => 'GET']);
            $most_viewed_products = array();
            foreach ($products as $product) {
                $flag = 0;
                foreach ($product as $attributes) {
                    $variants = array();
                    $variant_id = $attributes->variants[0]->id;
                    foreach ($attributes->variants as $variant) {
                        if ($variant->inventory_quantity > 0) {
                            $variants[$variant->id] = $variant->title;
                            $flag = 1;
                        }
                    }
                    if ($flag == 1) {
                        if (empty($attributes->images)) {
                            $img_src = 'https://zestardshop.com/shopifyapp/popular_products/public/image/no_image.png';
                        } else {
                            $img_src = $attributes->images[0]->src;
                        }
                        $data = array(
                            'store' => $shop,
                            'product_id' => $attributes->id,
                            'variants' => json_encode($variants),
                            'product_variant_id' => "$variant_id",
                            'product_name' => $attributes->title,
                            'product_price' => $attributes->variants[0]->price,
                            'product_image' => $img_src,
                            'product_handle' => $attributes->handle,
                            'product_description' => $attributes->body_html
                        );
                        array_push($most_viewed_products, $data);
                    }
                }
            }
            echo json_encode($most_viewed_products);
        }
    }

    public function get_products(Request $request) {
        $shop = $request->input('shop_name');
        $products = ProductsModel::where('store', $shop)->get()->toArray();
        return json_encode($products);
    }

    /* For Best Seller */

    public function save_session(Request $request) 
    {
        echo session('ids');
        echo $request->input('id');
        if ($request->input('flag') == 1) {
            if (empty(session('ids'))) {
                session(['ids' => $request->input('id')]);
            } else {
                $ids = session('ids') . "," . $request->input('id');
                session(['ids' => $ids]);
            }
        } else {
            if (empty(session('ids'))) {
                
            } else {
                $ids = session('ids');
                $array_ids = explode(",", $ids);
                $key = array_search($request->input('id'), $array_ids);
                $key;
                unset($array_ids[$key]);
                session(['ids' => implode(",", $array_ids)]);
            }
        }
    }

    public function update_product_ids(Request $request) {
        $updated_product_ids = $request->input('product_ids');
        session(['updated_product_ids' => implode(",", $updated_product_ids)]);
        echo session('updated_product_ids');
    }

    public function testing() {
        $shop = "thebigturk5.myshopify.com";
        $app_settings = DB::table('appsettings')->where('id', 1)->first();
        $select_store = DB::table('usersettings')->where('store_name', $shop)->first();
        $shop_id = $select_store->id;
        $encrypt = crypt($shop_id, "ze");
        $finaly_encrypt = str_replace(['/', '.'], "Z", $encrypt);
        $sh = App::make('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' => $app_settings->shared_secret, 'SHOP_DOMAIN' => $shop, 'ACCESS_TOKEN' => $select_store->access_token]);
        $theme = $sh->call(['URL' => '/admin/themes.json', 'METHOD' => 'GET']);
        foreach ($theme->themes as $themeData) {
            if ($themeData->role == 'main') {
                $snippets_arguments = ['id' => $finaly_encrypt];
                $theme_id = $themeData->id;
                $view = (string) View('snippets', $snippets_arguments);

                //api call for creating snippets
                $call = $sh->call(['URL' => '/admin/themes/' . $theme_id . '/assets.json', 'METHOD' => 'PUT', 'DATA' => ['asset' => ['key' => 'snippets/popular-products.liquid', 'value' => $view]]]);
            }
        }
        dd($call);
    }
}
