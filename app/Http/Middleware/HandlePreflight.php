<?php 
namespace App\Http\Middleware;

use Closure;

class HandlePreflight
{
    /** @var CorsService $cors */
    protected $cors;

    /* public function __construct(CorsService $cors)
    {
        $this->cors = $cors;
    } */

    /**
     * Handle an incoming Preflight request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
	    public function handle($request, Closure $next)
		{
			/*
			if ($request->getMethod() === "OPTIONS") {
				return response('');
			} 
			*/
			header('Access-Control-Allow-Origin: *');
			header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');
			header('Access-Control-Allow-Headers: Content-Type, Accept, Authorization, X-Requested-With, Application,X-Shopify-Web');
			return $next($request);
		}
  /*   public function handle($request, Closure $next)
    {
        if ($this->cors->isPreflightRequest($request)) {
            return $this->cors->handlePreflightRequest($request);
        }
        return $next($request);
    } */
}
